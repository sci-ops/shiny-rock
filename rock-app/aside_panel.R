aside_panel <- function() {
    
    tags_ <- tagList(
        withTags(
            div(class='sidepanel',
                span('Aside panel area'),
                div(class='line')
            )
        )
    )
    
    return(tags_)
    
}